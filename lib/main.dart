import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget imageSection = Container(
      child: Image.asset('assets/images/Diamond FF.jpg'),
    );

    Widget titleSection = Container(
      padding: EdgeInsets.only(top: 16),
      //margin: EdgeInsets.only(bottom: 16),
      child: Text(
        'Deskripsi Produk',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18,
        ),
      ),
    );

    Widget descriptionSection = Container(
      padding: EdgeInsets.all(16),
      child: Text(
        'Free Fire merupakan salah satu game terlaris saat ini. Oleh sebab itu, tidak dipungkiri lagi voucher game juga ikut laris manis. '
        'Diamond yang kami jual 100% legal dan cepat dalam pengirimannya.'
        'Oleh sebab itu, marilah top up game di IMBPN Store!!!',
        textAlign: TextAlign.center,
      ),
    );

    Widget rateSection = Row(
      children: [
        Icon(Icons.star, color: Colors.yellow),
        Icon(Icons.star, color: Colors.yellow),
        Icon(Icons.star, color: Colors.yellow),
        Icon(Icons.star, color: Colors.yellow),
        Icon(Icons.star),
      ],
    );

    Widget reviewSection = Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        rateSection,
        Text('1001 Ulasan'),
      ],
    );

    Widget _buildTextSection(
      String text,
      double textSize,
      double paddingTop,
    ) {
      return Container(
        padding: EdgeInsets.only(top: paddingTop),
        child: Text(
          text,
          style: TextStyle(
            fontSize: textSize,
          ),
        ),
      );
    }

    Widget _buildMenuSection(
      IconData iconData,
      String title,
      String timestamp,
    ) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(iconData),
          _buildTextSection(title, 16, 8),
          _buildTextSection(timestamp, 12, 12),
        ],
      );
    }

    Widget menuSection = Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        _buildMenuSection(Icons.favorite_border, "Wishlist", "999 Suka"),
        _buildMenuSection(Icons.timer, "Waktu Kirim", "1 - 10 Menit"),
        _buildMenuSection(Icons.shopping_cart_outlined, "Beli", "1200 terjual"),
        _buildMenuSection(Icons.chat_outlined, "Chat penjual", "100% direspon"),
      ],
    );

    return MaterialApp(
      title: 'IMBPN Store',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueAccent,
          leading: Icon(Icons.home, color: Colors.white),
          title: Text(
            'IMBPN Store',
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          actions: <Widget>[Icon(Icons.search, color: Colors.white)],
        ),
        body: ListView(
          children: [
            imageSection,
            titleSection,
            descriptionSection,
            Container(
              padding: EdgeInsets.only(bottom: 24),
              margin: EdgeInsets.only(top: 16),
              child: reviewSection,
            ),
            menuSection,
          ],
        ),
      ),
    );
  }
}
